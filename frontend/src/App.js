import React from 'react';
import { Route, BrowserRouter, Redirect, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap';
import 'jquery';
import 'popper.js';
import './App.css';

import Landing from './components/Landing';
import Dashboard from './components/Dashboard';
import ViewIssues from './components/ViewIssues';
import ModifyIssue from './components/ModifyIssue';
import AddIssue from './components/AddIssue';
import ChangeRoles from './components/ChangeRoles';


const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={Landing} />
        <Route exact path='/dashboard' component={Dashboard} />
        <Route exact path='/issues' component={ViewIssues} />
        <Route exact path='/modifyIssue/:issueId' component={ModifyIssue} />
        <Route exact path='/addIssue' component={AddIssue} />
        <Route exact path='/changeRoles' component={ChangeRoles} />
        <Redirect to='/dashboard' />
      </Switch>
    </BrowserRouter>
  )
};

export default App;
