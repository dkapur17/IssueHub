import React, { Component } from 'react'
import axios from 'axios';

import Loading from './Loading';
import NotAuth from './NotAuth';
import Navbar from './Navbar';

class Dashboard extends Component {
    state = {
        casData: null
    }
    componentDidMount() {
        axios.get('/api/currentUser').then(res => {
            this.setState({ casData: res.data });
        }).catch(err => {
            this.setState({ casData: { error: true } });
            console.log(err);
        });
    };
    render() {
        if (!this.state.casData)
            return <Loading />
        else if (this.state.casData.error)
            return <NotAuth />
        else
            return (
                <>
                    <Navbar role={this.state.casData.attributes.role} />
                    <div className='container mt-5'>
                        <h1 className='display-1 text-center'>Welcome, {this.state.casData.attributes.FirstName[0]}</h1>
                        <h3 className='text-center mt-5'> Use the navbar to go to your desired task.</h3>
                    </div>
                </>
            )
    }
};

export default Dashboard;
