import React from 'react';
import { Link } from 'react-router-dom';

const NotAuth = () => {
    return (
        <div className="container jumbotron mt-5" >
            <h1 className="display-4 text-center">You weren't authenticated by the server.</h1>
            <div className="row justify-content-center mt-5">
                <Link to='/' className='btn btn-outline-success'>Return to login page</Link>
            </div>
        </div>
    );
}

export default NotAuth;
