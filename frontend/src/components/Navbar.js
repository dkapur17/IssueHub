import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar = (props) => {

    let privilages = 0;
    if (props.role === 'ADMIN')
        privilages = 2;
    else if (props.role === 'EDITOR')
        privilages = 1;
    return (
        <div className='container-fluid'>
            <nav className="navbar navbar-expand-lg navbar-light bg-light row justify-content-between">
                <h4 className='mx-3 my-3'>Issue Hub</h4>
                <span>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item mx-2">
                                <NavLink className="nav-link" to="/dashboard">Dashboard</NavLink>
                            </li>
                            <li className="nav-item mx-2">
                                <NavLink className="nav-link" to="/issues">View Issues</NavLink>
                            </li>
                            {privilages > 0 ? <li className="nav-item mx-2">
                                <NavLink className="nav-link" to="/addIssue">Create New Issue</NavLink>
                            </li> : null}
                            {privilages > 1 ? <li className="nav-item mx-2">
                                <NavLink className="nav-link" to="/changeRoles">Change User Roles</NavLink>
                            </li> : null}
                            <li className="nav-item mx-5">
                                <a className="btn btn-outline-danger" href="http://localhost:5000/logout">Logout</a>
                            </li>
                        </ul>
                    </div>
                </span>
            </nav>
        </div>
    )
}

export default Navbar;
