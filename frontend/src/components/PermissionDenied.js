import React from 'react';
import { Link } from 'react-router-dom';

const PermissionDenied = () => {
    return (
        <div className="container jumbotron mt-5" >
            <h1 className="display-4 text-center">Looks like you don't have the permissions to access this page.</h1>
            <div className="row justify-content-center mt-5">
                <Link to='/dashboard' className='btn btn-outline-success'>Return to dashboard</Link>
            </div>
        </div>
    );
}

export default PermissionDenied;
