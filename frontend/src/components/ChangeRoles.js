import React, { Component } from 'react'
import axios from 'axios';

import Loading from './Loading';
import NotAuth from './NotAuth';
import PermissionDenied from './PermissionDenied';
import Navbar from './Navbar';

export class ChangeRoles extends Component {
    state = {
        casData: null,
        userDeets: {
            userId: null,
            role: 'VIEWER'
        },
        taskState: 0
    }
    componentDidMount() {
        axios.get('/api/currentUser').then(res => {
            this.setState({ casData: res.data });
        }).catch(err => {
            this.setState({ casData: { error: true } });
            console.log(err);
        });
    };

    handleChange = (e) => {
        let tempDeets = this.state.userDeets;
        tempDeets[e.target.id] = e.target.value;
        this.setState({ userDeets: tempDeets });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        axios.post('/api/modifyRole', {
            userDetails: this.state.userDeets
        }).then(res => {
            let stat = 0;
            if (res.data === "OK_CHANGE")
                stat = 1;
            else if (res.data === "NO_USER")
                stat = 2;
            else if (res.data === "PERM_DEN")
                stat = 3;
            else
                stat = 4;
            this.setState({ taskState: stat });
        }).catch(err => console.log(err));
    }

    render() {
        if (!this.state.casData)
            return <Loading />
        else if (this.state.casData.error)
            return <NotAuth />
        else if (this.state.casData.attributes.role !== "ADMIN")
            return <PermissionDenied />
        else {
            let alert;

            if (this.state.taskState === 0)
                alert = null;
            else if (this.state.taskState === 1)
                alert = (<div className='alert alert-success' role="alert">
                    Successfully changed User's Role!
                </div>);
            else if (this.state.taskState === 2)
                alert = (<div className='alert alert-warning' role="alert">
                    No such user.
                </div>);
            else if (this.state.taskState === 3)
                alert = (<div className='alert alert-danger' role="alert">
                    Permission Denied.
                </div>);
            else
                alert = (<div className='alert alert-danger' role="alert">
                    Unable to change user's role. Database Error!
                </div>);
            return (
                <>
                    <Navbar role={this.state.casData.attributes.role} />
                    <div className="container">
                        <h1 className='display-2 text-center my-3'>Change User Role</h1>
                        <form>
                            <div className="form-group">
                                <label htmlFor="userId">UserID</label>
                                <input type="email" className="form-control" id="userId" aria-describedby="emailHelp" onChange={this.handleChange} />
                                <small id="emailHelp" className="form-text text-muted">This is the User's CAS Login ID</small>
                            </div>
                            <select className="form-control" id='role' onChange={this.handleChange}>
                                <option>VIEWER</option>
                                <option>EDITOR</option>
                                <option>ADMIN</option>
                            </select>
                            <div className="row mb-3 justify-content-around">
                                <button type="submit" className="btn btn-outline-danger mt-3" onClick={() => this.props.history.push('/dashboard')}>Cancel</button>
                                <button type="submit" className="btn btn-outline-primary mt-3" onClick={this.handleSubmit}>Submit</button>
                            </div>
                        </form>
                        {alert}
                    </div>
                </>
            )
        }
    }
}

export default ChangeRoles;
