import React from 'react';

const Landing = (props) => {
    return (
        <div className="container jumbotron mt-5" >
            <h1 className="display-1 text-center">Welcome to IssueHub</h1>
            <form action="http://localhost:5000/login" method="get" className='row justify-content-center mt-5'>
                <button type="submit" className="btn btn-outline-success px-5 py-3">Sign In</button>
            </form>
        </div>
    )
}

export default Landing;
