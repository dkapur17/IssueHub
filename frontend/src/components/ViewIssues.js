import React, { Component } from 'react';
import axios from 'axios';

import Loading from './Loading';
import NotAuth from './NotAuth';
import Navbar from './Navbar';
import IssueCard from './IssueCard';

export class ViewIssues extends Component {
    state = {
        casData: null,
        issues: null
    }
    componentDidMount() {
        axios.get('/api/currentUser').then(res => {
            this.setState({ casData: res.data });
        }).catch(err => {
            this.setState({ casData: { error: true } });
            console.log(err);
        });
        axios.get('/api/issues').then(res => {
            this.setState({ issues: res.data });
        }).catch(err => {
            console.log(err);
        })
    };
    render() {

        if (!this.state.casData)
            return <Loading />
        else if (this.state.casData.error)
            return <NotAuth />
        else {
            let issueList = [];
            if (this.state.issues !== null)
                issueList = this.state.issues.map(issue => <IssueCard key={issue._id} issueData={issue} role={this.state.casData.attributes.role} />);
            const issueCards = this.state.issues === null ? <Loading /> : this.state.issues.length > 0 ? <div className='row'>{issueList}</div>
                : <h3 className='display-4 text-center'>Seems like all the issues have been taken care of.</h3>;
            return (
                <>
                    <Navbar role={this.state.casData.attributes.role} />
                    <h1 className='display-2 text-center my-3'>Issues</h1>
                    <div className="container-fluid text-center">
                        {issueCards}
                    </div>
                </>
            )
        }
    }
}

export default ViewIssues
