import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

const IssueCard = (props) => {
    let privilages = 0;
    if (props.role === 'ADMIN')
        privilages = 2;
    else if (props.role === 'EDITOR')
        privilages = 1;

    let issue = props.issueData;

    const deleteIssue = () => {
        axios.post('/api/deleteIssue', {
            issueId: issue._id
        });
        window.location.reload();

    }
    const modifyIssue = () => {
        props.history.push(`/modifyIssue/${issue._id}`);
    }

    return (
        <div className="card mx-5 my-3" style={{ width: '18rem' }}>
            <div className="card-body">
                <h5 className="card-title my-2 text-center">{issue.title}</h5>
                {issue.tags.map((tag, index) => <span className="badge badge-info mx-1" key={index}>{tag}</span>)}
                <p className="card-text mt-4">{issue.desc}</p>
                <p><b>Assignees:</b> {issue.assignees.length === 0 ? <span>None</span> : issue.assignees.map((assignee, index) => <span key={index}>{assignee}{index < issue.assignees.length - 1 ? ", " : " "}</span>)}</p>
                <p><b>State: </b>{issue.state}</p>
                {privilages > 0 ? <div className='row justify-content-around'>
                    <button className='btn btn-outline-warning mx-1' onClick={modifyIssue}>Modify</button>
                    <button className='btn btn-outline-danger mx-1' onClick={deleteIssue}>Delete</button>
                </div> : null}
            </div>
        </div >
    )
}

export default withRouter(IssueCard);
