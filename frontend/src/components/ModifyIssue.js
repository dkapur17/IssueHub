import React, { Component } from 'react';
import Navbar from './Navbar';
import axios from 'axios';

import Loading from './Loading';
import NotAuth from './NotAuth';
import PermissionDenied from './PermissionDenied';

export class ModifyIssue extends Component {
    state = {
        casData: null,
        issue: null
    }
    componentDidMount() {
        axios.get('/api/currentUser').then(res => {
            this.setState({ casData: res.data });
        }).catch(err => {
            this.setState({ casData: { error: true } });
            console.log(err);
        });
        axios.post('/api/issueInfo', {
            issueId: this.props.match.params.issueId
        }).then(res => {
            this.setState({ issue: res.data });
        }).catch(err => {
            console.log(err);
        })
    };
    handleChange = (e) => {
        let tempIssue = this.state.issue;
        if (e.target.id === 'assignees' || e.target.id === 'tags')
            tempIssue[e.target.id] = e.target.value.split(",");
        else
            tempIssue[e.target.id] = e.target.value;
        this.setState({ issue: tempIssue });
    }
    handleSubmit = (e) => {
        e.preventDefault();
        axios.post('/api/modifyIssue', {
            issue: this.state.issue
        })
        this.props.history.push('/issues');
    }
    render() {
        if (!this.state.casData || !this.state.issue)
            return <Loading />
        else if (this.state.casData.error)
            return <NotAuth />
        else if (this.state.issue === {})
            return <div>Invalid Issue Id</div>
        else {
            if (this.state.casData.attributes.role === "VIEWER")
                return <PermissionDenied />
            else
                return (
                    <>
                        <Navbar role={this.state.casData.attributes.role} />
                        <div className="container">
                            <h1 className='display-2 text-center my-3'>Modify Issue</h1>
                            <div className="alert alert-warning" role="alert">
                                The changes may take a while to show up.
                            </div>
                            <form>
                                <div className="form-group">
                                    <label htmlFor="title">Title</label>
                                    <input type="text" className="form-control" id="title" value={this.state.issue.title} onChange={this.handleChange} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="desc">Description</label>
                                    <input type="text" className="form-control" id="desc" value={this.state.issue.desc} onChange={this.handleChange} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="assignees">Assignees</label>
                                    <input type="text" className="form-control" id="assignees" value={this.state.issue.assignees.join()} onChange={this.handleChange} />
                                    <small className="form-text text-muted">Comma separate multiple names</small>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="tags">Tags</label>
                                    <input type="text" className="form-control" id="tags" value={this.state.issue.tags.join()} onChange={this.handleChange} />
                                    <small className="form-text text-muted">Comma separate multiple tags</small>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="state">State</label>
                                    <select className="form-control" id="state" onChange={this.handleChange}>
                                        <option>OPEN</option>
                                        <option>ASSIGNED</option>
                                        <option>FIXED</option>
                                        <option>ABANDONED</option>
                                    </select>
                                </div>

                                <div className="row mb-3 justify-content-around">
                                    <button type="submit" className="btn btn-outline-danger mt-3" onClick={() => this.props.history.push('/issues')}>Cancel</button>
                                    <button type="submit" className="btn btn-outline-primary mt-3" onClick={this.handleSubmit}>Submit</button>
                                </div>
                            </form>
                        </div>
                    </>
                )
        }
    }
}

export default ModifyIssue;
