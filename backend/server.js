const express = require('express');
const cas = require('connect-cas');
const url = require('url');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const MemoryStore = require('session-memory-store')(session);
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const Issues = require('./models/Issue');
const Users = require('./models/User');

cas.configure({ 'host': 'login.iiit.ac.in' });
mongoose.connect('mongodb+srv://webadmin:webadmin@issuehub.xkoqn.mongodb.net/IssueHub?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true }, () => console.log("Connected to MongoDB"));

const app = express();
const port = 5000;

app.use(cookieParser());
app.use(session({
    name: 'NSESSIONID',
    secret: 'th1s1s4s3ss10ns3cr3t',
    store: new MemoryStore(),
    resave: true,
    saveUninitialized: true
}));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/login', cas.serviceValidate(), cas.authenticate(), (req, res) => {
    if (req.session.cas && req.session.cas.user) {
        Users.findOne({ userId: req.session.cas.user }, (err, user) => {
            if (err)
                console.log("There was an error in getting user from the database");
            else
                if (!user) {
                    let newUser = new Users({
                        userId: req.session.cas.user,
                        role: req.session.cas.user === "dhruv.kapur@students.iiit.ac.in" ? "ADMIN" : "VIEWER"
                    });
                    newUser.save();
                }
        });
        return res.redirect('http://localhost:3000/dashboard');
    }
});

app.get('/logout', (req, res) => {
    if (!req.session)
        return res.redirect('/');
    if (req.session.destroy)
        req.session.destroy();
    else
        req.session = null;

    let options = cas.configure();
    options.pathname = options.paths.logout;
    return res.redirect(url.format(options));
});

app.get('/api/currentUser', (req, res) => {
    Users.findOne({ userId: req.session.cas.user }, (err, user) => {
        if (err)
            console.log("There was an error in getting user from the database");
        else {
            req.session.cas.attributes.role = user.role;
            return res.send(req.session.cas ? req.session.cas : { error: true });
        }
    });
});

app.post('/api/deleteIssue', (req, res) => {
    const issueId = req.body.issueId;
    Issues.findById(issueId, function (err, doc) {
        if (err)
            console.log("Unable to find issue");
        doc.remove();
    })
    return res.send("DELETED");
});

app.post('/api/modifyIssue', (req, res) => {
    const modifiedIssue = req.body.issue;
    Issues.findById(modifiedIssue._id, (err, issue) => {
        issue.update({
            title: modifiedIssue.title,
            desc: modifiedIssue.desc,
            assignees: modifiedIssue.assignees,
            tags: modifiedIssue.tags,
            state: modifiedIssue.state,
        }).exec();
    });
});

app.post('/api/modifyRole', (req, res) => {
    const modifiedUser = req.body.userDetails;
    if (modifiedUser.userId === 'dhruv.kapur@students.iiit.ac.in')
        return res.send("PERM_DEN");
    Users.findOne({ userId: modifiedUser.userId }, (err, user) => {
        if (err)
            return res.send("DB_ERR");
        else if (!user)
            return res.send("NO_USER");
        else {
            user.update({
                role: modifiedUser.role
            }).exec();
            return res.send("OK_CHANGE");
        }
    });
})

app.post('/api/addIssue', (req, res) => {
    let issue = new Issues({
        title: req.body.issue.title,
        desc: req.body.issue.desc,
        assignees: req.body.issue.assignees,
        tags: req.body.issue.tags,
        state: req.body.issue.state
    });
    issue.save().then(res => res.send("ADDED")).catch(err => res.send("NOT_ADDED"));
})


app.get('/api/issues', (req, res) => {
    Issues.find((err, issues) => {
        if (err)
            console.error(err);
        else
            return res.send(issues);
    })
});

app.post('/api/issueInfo', (req, res) => {
    Issues.findById(req.body.issueId, (err, issue) => {
        if (err)
            return {};
        else
            return res.send(issue);
    })
});

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});