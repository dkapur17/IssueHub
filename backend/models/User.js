const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    userId: String,
    role: String
});

const Users = mongoose.model('Users', UserSchema, 'Users');

module.exports = Users;