const mongoose = require('mongoose');

const IssueSchema = new mongoose.Schema({
    title: String,
    desc: String,
    assignees: Array,
    tags: Array,
    state: String
});

const Issues = mongoose.model('Issues', IssueSchema, 'Issues');

module.exports = Issues;