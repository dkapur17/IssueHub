# IssueHub
Web app for web admin recruitment.

A crude implementation of the requirements. To run, first clone the repo, then run the following commands:

```bash
cd IssueHub
npm i
npm i --prefix frontend
npm i --prefix backend
npm start
```
**Note**: You must have `node` and `npm` installed to run this web app.

The frontend is written in React, the backend in Express and the database uses MongoDB.
To reduce setup headache, an instance of MongoDB Atlas cloud DB has been used.
The credentials for the database have been hardcoded into the script (yes, I'm aware that's a security vulnerability, but this web app is for testing purposes only and not for deployment, so please write to the database with that in mind).
